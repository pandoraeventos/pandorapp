Rails.application.routes.draw do


  devise_for :users, controllers: {
      sessions: 'users/sessions',
      confirmations: 'users/confirmations'
  }

=begin
  namespace :api do
    namespace :v1 do
      resources :users, only: [:index, :create, :destroy, :update], defaults: { format: 'json' }
      resources :events, only: [:index, :create, :destroy, :update], defaults: { format: 'json' }
      resources :tasks, only: [:index, :create, :destroy, :update], defaults: { format: 'json' }
      resources :stands, only: [:index, :create, :destroy, :update], defaults: { format: 'json' }
      resources :reserves, only: [:index, :create, :destroy, :update], defaults: { format: 'json' }
      resources :merchants, only: [:index, :create, :destroy, :update], defaults: { format: 'json' }
    end
  end
=end

  root to: 'landing#index'

  resources :events
  #resources :stands
  resources :reserves, only: [:index,:show,:update,:edit,:destroy]
  resources :personal_informations, only: [:create, :new, :edit, :update]
  resources :merchants, only: [:index, :create, :update, :edit, :new]

  post 'events/toggle_stand_auction', to: 'events#toggle_stand_auction'
  get 'events/subscribe/:hash', to: 'events#subscribe_stand'
  get 'reserves/all/:event', to: 'reserves#all'
  get 'reserves/about_to_expire/:id', to: 'reserves#about_to_expire'
  get 'reserves/expire/:id', to: 'reserves#expire'
  post 'reserves/payment', to: 'reserves#payment'

  post 'payment_email', to: 'payments#payment_email'
  post 'payment_upload', to: 'payments#payment_upload'
  get 'payment_status/:id', to: 'payments#payment_status'
  get 'payment_toggle/:id', to: 'payments#payment_toggle'
  get 'payment_confirmation/:id', to: 'payments#payment_confirm'

  get 'mercado_payment/:id', to: 'mercado_pago#status'
  post 'mercado_pago/ipn', to: 'mercado_pago#process_notification'
  get 'mercado_pago/failure', to: 'mercado_pago#failure'
  get 'mercado_pago/success', to: 'mercado_pago#success'
  get 'mercado_pago/pending', to: 'mercado_pago#pending'
  get 'mercado_pago/payment_info/:id', to: 'mercado_pago#get_payment_info_test'

  #get 'tasks', to: 'tasks#dashboard'

  get 'users', to: 'users#index'
  get 'users/new_more', to: 'users#personal_info'

  get 'wizard/:event', to: 'step#step'
  get 'wizard/:event/edit/:step', to: 'step#edit_step'



  get 'landing/institutional', to: 'landing#institutional'
  get 'landing/events', to: 'landing#events'
  get 'landing/multimedia', to: 'landing#multimedia'
  get 'landing/contact', to: 'landing#contact'

  get 'helpers', to: 'landing#helpers'

  get 'token', to: 'mercado_pago#index'
  get 'payment/:id', to: 'mercado_pago#get_payment_info'

  post 'landing/contact', to: 'mailer#contact'

  match "/404", :to => 'errors#not_found', :via => :all
  match "/500", :to => 'errors#internal_server_error', :via => :all
  get 'errors/not_found'
  get 'errors/internal_server_error'

end
