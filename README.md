# README #

Main aplication for Pandora's task management.

### Ruby version ###
Install latest version of rails and ruby

### System dependencies ###
Heroku toolbelt
PostgresSQL 

### Configuration ###
Type "gem install bundler tzinfo-data" and now "bundle install"

### Database creation ###
"createdb -U postgres pandorapp_development" (teke notes of your user/password)
psql -U postgres -c "CREATE ROLE testDev LOGIN NOSUPERUSER INHERIT CREATEDB CREATEROLE;" pandorapp_development

On config\database.yml add your PostgresSQL db/user/password
´  
<<: *default
  database: pandorapp_development
  username: postgres
  password: postgresPass
  ´

### Database initialization ###
rake db:migrate

rake db:seed 


### Deployment instructions ###
Type "rails s"
Now open "localhost:3000" on your browser

### Bcrypt 500 error ###
On bash run:
´
gem list bcrypt | cut -d" " -f1 | xargs gem uninstall -aIx
gem install bcrypt --platform=ruby
´

* ...