# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Comment the lines if you already execute that seed.

#roles = Role.create([{name: 'Admin'}, {name: 'Event Coordinator'}, {name: 'User'}, {name: 'Cosplayer'},{name: 'Stand'}])

#categories = Category.create([{type_name: 'Merchandising'}, {type_name: 'Comestibles'}, {type_name: 'Figuras'}, {type_name: 'Grafica'}, { type_name: 'Manualidades'}, {type_name: 'Textil'}])
#
#

stands = Stand.create([{name:'P1',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P2',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P3',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P4',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P5',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P6',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P7',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P8',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P9',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P10',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P11',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'P12',sector:'scenario',event_id:1,price:'650',status:'Libre'},
                       {name:'N1',sector:'alley',event_id:1,price:'450',status:'Libre'},
                       {name:'N2',sector:'alley',event_id:1,price:'450',status:'Libre'},
                       {name:'N3',sector:'alley',event_id:1,price:'450',status:'Libre'},
                       {name:'N4',sector:'alley',event_id:1,price:'450',status:'Libre'},
                       {name:'N5',sector:'alley',event_id:1,price:'450',status:'Libre'},
                       {name:'N6',sector:'alley',event_id:1,price:'450',status:'Libre'},
                      ])