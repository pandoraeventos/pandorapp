class AddReservePaymentToReserves < ActiveRecord::Migration[5.1]
  def change
    add_column :reserves, :mail_sent, :datetime
    add_column :reserves, :payment_confirmation, :datetime
  end
end
