class AddMpIdToPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :mp_id, :string
  end
end
