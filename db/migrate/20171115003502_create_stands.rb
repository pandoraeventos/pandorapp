class CreateStands < ActiveRecord::Migration[5.1]
  def change
    create_table :stands do |t|
      t.string :sector
      t.string :status
      t.references :event, index: true, foreign_key: true
      t.float :price
      t.string :size

      t.timestamps null: false
    end
  end
end
