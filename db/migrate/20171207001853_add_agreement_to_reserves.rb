class AddAgreementToReserves < ActiveRecord::Migration[5.1]
  def change
    add_column :reserves, :agreement, :boolean, default: 'false'
  end
end
