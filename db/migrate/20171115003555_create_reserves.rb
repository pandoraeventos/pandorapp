class CreateReserves < ActiveRecord::Migration[5.1]
  def change
    create_table :reserves do |t|
      t.references :stand, index: true, foreign_key: true
      t.references :merchant, index: true, foreign_key: true
      t.string :payment

      t.timestamps null: false
    end
  end
end
