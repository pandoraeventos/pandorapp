class ChangePaymentsFromReserve < ActiveRecord::Migration[5.1]
  def change
    add_reference :reserves, :payment
    remove_column :reserves, :payment
  end
end
