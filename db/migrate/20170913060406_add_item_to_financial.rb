class AddItemToFinancial < ActiveRecord::Migration[5.1]
  def change
    add_reference :financials, :item, index: true, foreign_key: true
  end
end
