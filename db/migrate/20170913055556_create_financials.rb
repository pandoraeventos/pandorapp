class CreateFinancials < ActiveRecord::Migration[5.1]
  def change
    create_table :financials do |t|
      t.string :type
      t.date :date
      t.integer :status
      t.float :amount
      t.references :user, index: true, foreign_key: true
      t.references :task, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
