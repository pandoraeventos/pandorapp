class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.string :type
      t.datetime :start_date
      t.datetime :finish_date
      t.integer :owner
      t.integer :creator
      t.datetime :deadline
      t.integer :status
      t.integer :estimation
      t.integer :priority
      t.text :tags
      t.datetime :update_at
      t.references :event, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
