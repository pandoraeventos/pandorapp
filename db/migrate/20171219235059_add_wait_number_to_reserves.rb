class AddWaitNumberToReserves < ActiveRecord::Migration[5.1]
  def change
    add_column :reserves, :wait_number, :integer , default: 0
  end
end
