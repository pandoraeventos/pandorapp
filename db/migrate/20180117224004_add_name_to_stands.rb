class AddNameToStands < ActiveRecord::Migration[5.1]
  def change
    add_column :stands, :name, :string
  end
end
