class CreatePersonalInformations < ActiveRecord::Migration[5.1]
  def change
    create_table :personal_informations do |t|
      t.string :name
      t.string :last_name
      t.string :dni
      t.string :contact
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
