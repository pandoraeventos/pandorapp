class CreateTaskComments < ActiveRecord::Migration[5.1]
  def change
    create_table :task_comments do |t|
      t.references :task, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.text :description

      t.timestamps null: false
    end
  end
end
