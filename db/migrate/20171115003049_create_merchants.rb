class CreateMerchants < ActiveRecord::Migration[5.1]
  def change
    create_table :merchants do |t|
      t.string :store_name
      t.references :personal_information, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true
      t.string :web_page
      t.string :city

      t.timestamps null: false
    end
  end
end
