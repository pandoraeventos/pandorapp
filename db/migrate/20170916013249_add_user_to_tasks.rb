class AddUserToTasks < ActiveRecord::Migration[5.1]
  def change
    add_reference :tasks, :user
    remove_column :tasks, :owner
  end
end
