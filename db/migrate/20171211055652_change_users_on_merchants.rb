class ChangeUsersOnMerchants < ActiveRecord::Migration[5.1]
  def change
    rename_column :merchants, :users_id, :user_id
  end
end
