class AddFieldsToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column  :events, :address, :string, :default => ''
    add_column  :events, :city, :string, :default => ''
    add_column  :events, :facebook, :string, :default => ''
  end
end
