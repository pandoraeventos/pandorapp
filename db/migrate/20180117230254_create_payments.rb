class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.date :payment_date
      t.text :preference_id
      t.float :amount
      t.text :status
      t.integer :payment_type
      t.text :payment_confirmation_img
    end
  end
end