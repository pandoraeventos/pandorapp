class AddEventToReserves < ActiveRecord::Migration[5.1]
  def change
    add_reference :reserves, :event ,index: true
  end
end
