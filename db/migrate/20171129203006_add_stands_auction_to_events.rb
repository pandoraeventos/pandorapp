class AddStandsAuctionToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :stand_auction, :boolean, default: false
    add_column :events, :stand_hash_link, :string
  end
end
