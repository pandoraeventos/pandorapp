class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :type
      t.integer :quantity
      t.string :category
      t.integer :place

      t.timestamps null: false
    end
  end
end
