class CreateTaskDependencies < ActiveRecord::Migration[5.1]
  def change
    create_table :task_dependencies do |t|
      t.integer :first
      t.integer :second
      t.integer :type

      t.timestamps null: false
    end
  end
end
