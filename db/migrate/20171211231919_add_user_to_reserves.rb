class AddUserToReserves < ActiveRecord::Migration[5.1]
  def change
    add_reference :reserves, :user, foreign_key: true
  end
end
