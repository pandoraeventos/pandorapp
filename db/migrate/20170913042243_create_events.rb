class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.text :name
      t.date :release_date

      t.timestamps null: false
    end
  end
end
