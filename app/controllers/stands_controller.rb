class StandsController < ApplicationController
  layout 'main_app'
  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :check_privileges!
  before_action :load_user

  def index

  end

  def create

  end

  def update

  end

  private def stands_params
    params.require(:stand).permit(:id, :sector, :status, :event_id , :price, :size, :name)
  end
end