class StepController < ApplicationController
  layout 'main_app'
  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :load_user
  FINAL_STEP = 3
  MAX_RESERVES = 16

  def step
    redirect_to(events_path, :notice => 'Event id not found') and return if params[:event].nil?

    @reserve = Reserve.find_by(user_id: current_user.id, event_id: params[:event])
    redirect_to(events_path, :notice => 'You don\'t have any reserve for this event yet') and return unless @reserve
    unless @reserve.agreement

      @step = get_current_step
      @step_text = @step == FINAL_STEP ? 'Finalizar' : 'Siguiente'
      @step_form = get_form (@step)
      if @step == FINAL_STEP
        @show_warning = false
        limit = Reserve.where(event_id: params[:event], agreement:true)
        if limit.length >= MAX_RESERVES
          @show_warning = true
        end
      end
      initialize_step(@step)
      render 'merchants/wizard' and return
    end
    redirect_to @reserve
  end

  def edit_step
    redirect_to(events_path, :notice => 'Event id not found') and return if params[:event].nil?

    @reserve = Reserve.find_by(user_id: current_user.id, event_id: params[:event])
    redirect_to(events_path, :notice => 'You don\'t have any reserve for this event yet') and return unless @reserve

    unless @reserve.agreement
      @step = get_current_step
      redirect_to('/wizard/'+params[:event]) and return if params[:step].nil? || !valid_step?(params[:step], @step)
      @step_text = params[:step].to_i == FINAL_STEP ? 'Finalizar' : 'Siguiente'
      @step_form = get_form (params[:step].to_i)
      initialize_step(params[:step].to_i)
      render 'merchants/wizard' and return
    end
    redirect_to @reserve
  end

  private def get_current_step
    step = 1
    @personal_info = PersonalInformation.find_by_user_id (current_user.id)
    if @personal_info
      step = 2
      @merchant = Merchant.find_by_user_id (current_user.id)
      @categories = Category.all
      if @merchant
        step = 3
      end
    end
    step
  end

  private def get_form (step)
    case
      when step == 1 then 'users/personal_info'
      when step == 2 then 'merchants/form'
      when step == 3 then 'reserves/agreements'
      else
        @reserve
    end
  end

  private def get_form_name (step)
    case
      when step == 1 then 'Info Personal'
      when step == 2 then 'Info Stand'
      when step == 3 then 'Términos/Condiciones'
      else
        'Error'
    end
  end
  helper_method :get_form_name

  private def initialize_step (step)
    @data_model
    case
      when step == 1 then @data_model = @personal_info ? @personal_info: PersonalInformation.new
      when step == 2 then @data_model = @merchant ? @merchant : Merchant.new
      when step == 3 then @data_model = @reserve ? @reserve : Reserve.new
      else
        'reserve/done'
    end
  end

  private def valid_step? (step, current)
    (step.to_i.is_a? Integer) && (step.to_i.between?(1,current)) && (step != 4)
  end
end