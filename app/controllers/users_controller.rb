class UsersController < ApplicationController
  layout 'main_app'
  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :load_user

  def index
      @personal_info = PersonalInformation.find_by_user_id current_user.id
  end

  def personal_info
    @data_model = PersonalInformation.new
    render 'users/new' and return
  end
end
