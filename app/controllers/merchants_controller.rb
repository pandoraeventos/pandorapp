class MerchantsController < ApplicationController
  layout 'main_app'
  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :load_user

  def index
    @data_model = Merchant.find_by_user_id(current_user.id)
    redirect_to (new_merchant_path) unless @data_model
  end

  def create
    @merchant = Merchant.new(merchant_params)
    @merchant.user_id = current_user.id
    @merchant.personal_information_id = PersonalInformation.find_by_user_id(current_user.id).id
    if @merchant.save
      if request.referer.include? 'wizard'
        redirect_to request.referer.scan(/\/wizard\/[0-9]+/).last
      else
        redirect_to merchants_path
      end
    else
      render 'new'
    end
  end

  def new
    @data_model = Merchant.find_by_user_id(current_user.id)
    redirect_to edit_merchant_path(@data_model) and return if @data_model
    @data_model = Merchant.new
    @categories = Category.all
  end

  def edit
    @data_model = Merchant.find_by_user_id(current_user.id)
    redirect_to(merchants_path, :notice => 'Merchant not found') unless @data_model
    @categories = Category.all
  end

  def update
    @merchant = Merchant.find(params[:id])
    if @merchant.update(merchant_params)
      flash[:notice] = 'Merchant Information was successfully updated.'
      if request.referer.include? 'wizard'
        redirect_to request.referer.scan(/\/wizard\/[0-9]+/).last
      else
        redirect_to merchants_path
      end
    else
      render 'edit'
    end
  end

  def destroy
    @merchant = Merchant.find(params[:id])
    @merchant.destroy

    redirect_to merchants_path
  end

  private def merchant_params
    params.require(:merchant).permit(:id, :store_name, :web_page, :city, :category_id)
  end
end