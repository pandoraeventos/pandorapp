class Api::V1::MerchantsController < Api::V1::BaseController

  def index
    respond_with Merchant.all
  end

  def create
    respond_with :api, :v1, Merchant.create(merchant_params)
  end

  def destroy
    respond_with Merchant.destroy(params[:id])
  end

  def update
    merchant = Merchant.find(params['id'])
    merchant.update_attributes(merchant_params)
    respond_with merchant, json: event
  end

  private def merchant_params
    params.require(:stand).permit(:id, :store_name, :status, :event_id, :size, :price, :personal_information)
  end
end

