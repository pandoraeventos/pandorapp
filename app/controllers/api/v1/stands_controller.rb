class Api::V1::StandsController < Api::V1::BaseController

  def index
    respond_with Stand.all
  end

  def create
    respond_with :api, :v1, Stand.create(stand_params)
  end

  def destroy
    respond_with Stand.destroy(params[:id])
  end

  def update
    stand = Stand.find(params['id'])
    stand.update_attributes(stand_params)
    respond_with stand, json: event
  end

  private def stand_params
    params.require(:stand).permit(:id, :sector, :status, :event_id, :size, :price)
  end
end

