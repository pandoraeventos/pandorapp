class Api::V1::TasksController < Api::V1::BaseController
  #before_filter :authenticate_user!
  #before_filter :check_privileges!

  def index
    respond_with Task.all
  end

  def create
    respond_with :api, :v1, Task.create(task_params)
  end

  def destroy
    respond_with Task.destroy(params[:id])
  end

  def update
    task = Task.find(params['id'])
    task.update_attributes(task_params)
    respond_with task, json: task
  end

  private def task_params
    params.require(:task).permit(:id, :title, :description, :payment_type, :start_date, :finish_date, :deadline, :status, :estimation, :priority, :tags, :user_id)
  end
end
