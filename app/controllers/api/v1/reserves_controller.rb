class Api::V1::ReservesController < Api::V1::BaseController

  def index
    respond_with Reserve.all
  end

  def create
    respond_with :api, :v1, Reserve.create(reserve_params)
  end

  def destroy
    respond_with Reserve.destroy(params[:id])
  end

  def update
    reserve = Reserve.find(params['id'])
    reserve.update_attributes(stand_params)
    respond_with reserve, json: event
  end

  private def reserve_params
    params.require(:stand).permit(:id, :merchant_id, :stand_id)
  end
end

