class MercadoPagoController < ApplicationController
  layout 'main_app'

  protect_from_forgery with: :null_session
  before_action :authenticate_user!, except: [:process_notification]
  before_action :check_privileges! , only: [:get_payment_info_test]
  before_action :load_user
  include MercadoPagoHelper
  def index
    @token = get_token
  end

  def status
    get_reserve
    redirect_to(reserves_path, :notice => 'Reserva no encontrada.') and return unless @reserve
    redirect_to(reserves_path, :notice => 'No se registran confirmaciones de la reserva aun.') and return if @reserve.payment_confirmation.nil?
    redirect_to(reserves_path) and return unless @reserve.payment.payment_date.nil?

    if @reserve.payment.preference_id.nil?
      @token = create_payment_token @reserve
      if @token['status'] == '201'
        @status = 'Ticket de MercadoPago creado.'
        @reserve.payment.preference_id = @token['response']['id']
        @reserve.payment.status = @status
        @reserve.payment.save
      else
        @status = 'Error al crear ticket de MercadoPago, vuelva a intentar mas tarde.'
        #mail a pandora avisando del error
      end
    else
      @status = @reserve.payment.status
      @token = get_preference_token @reserve.payment.preference_id
    end

    @mercado_url = @token && ENVIRONMENT != 'http://www.pandoraboxeventos.com' ? @token['response']['sandbox_init_point'] : @token['response']['init_point']

    @hide_payment_button = true
    render 'payments/mercado_payment'
  end

  def process_notification
    #?topic=payment&id=123456789
    if params['topic'] == 'payment'
      payment_info = get_payment_info params['id']
      unless payment_info.nil?
        @reserve = Reserve.find(payment_info['collection']['external_reference'])
        unless @reserve.payment.payment_date.nil?
          case payment_info['collection']['status']
            when 'approved'
              approve payment_info['collection']['id']
            when 'cancelled'
              @reserve.payment.status = 'Cancelada en MercadoPago'
              @reserve.payment.save
            else
              # type code here
          end
        end
      end
    end
    render json: {status: :ok }
  end


  def get_payment_info_test
    @token = get_payment_info params['id']
    render 'index'
  end

  def failure
    redirect_to ('/mercado_payment/'+params[:external_reference].to_s)
  end

  def success
    @reserve = Reserve.find(params[:external_reference])
    redirect_to(reserves_path, :notice => 'Reserva no encontrada.') and return unless @reserve
    redirect_to(reserves_path, :notice => 'No se registran confirmaciones de la reserva aun.') and return if @reserve.payment_confirmation.nil?
    redirect_to(reserves_path) and return unless @reserve.payment.payment_date.nil?

    approve (params[:collection_id])

    redirect_to ('/reserves/'+params[:external_reference].to_s)
  end

  def pending
    @reserve = Reserve.find(params[:external_reference])
    redirect_to(reserves_path, :notice => 'Reserva no encontrada.') and return unless @reserve
    redirect_to(reserves_path, :notice => 'No se registran confirmaciones de la reserva aun.') and return if @reserve.payment_confirmation.nil?
    redirect_to(reserves_path) and return unless @reserve.payment.payment_date.nil?

    @reserve.payment.status = 'Pendiente en MercadoPago'
    @reserve.payment.mp_id = params[:collection_id]
    @reserve.payment.save

    redirect_to ('/mercado_payment/'+params[:external_reference].to_s)
  end

  private def get_reserve
    @reserve = Reserve.where(user_id: current_user.id, id: params[:id]).first
  end

  private def approve (collection)
    @reserve.payment.status = 'Finalizado'
    @reserve.payment.mp_id = collection
    @reserve.payment.payment_date = Time.now
    @reserve.payment.save
    PandoraMailer.reserve_payment_upload(@reserve, request).deliver
    PandoraMailer.reserve_complete(@reserve, request).deliver
  end
end