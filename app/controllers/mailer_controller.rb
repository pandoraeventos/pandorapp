class MailerController < ApplicationController
  protect_from_forgery with: :null_session

  def contact
    name = params[:name]
    email = params[:email]
    body = params[:body]

    PandoraMailer.contact_email(name, email, body, request).deliver
    flash[:notice] = 'Mensaje enviado, gracias por contactarse con nosotros.'
    render landing_contact_path
  end

end