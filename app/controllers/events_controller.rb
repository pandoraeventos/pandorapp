class EventsController < ApplicationController
  layout 'main_app'
  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :check_privileges! , except: [:subscribe_stand, :index]
  before_action :load_user

  def index
    if (current_user.admin)
      @events = Event.all
    else
      @events = Event.where(stand_auction: true);
    end
  end

  def show
    unless params[:id].nil?
      @event = Event.find_by_id params[:id]
      redirect_to(events_path, :notice => 'Event not found') unless @event
    end
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      generate_hashlinks
      redirect_to @event
    else
      render 'new'
    end
  end

  def new
    @event = Event.new
  end

  def edit
    unless params[:id].nil?
      @event = Event.find_by_id params[:id]
      redirect_to(events_path, :notice => 'Event not found') unless @event
    end
  end

  def update
    @event = Event.find(params[:id])

    if @event.update(event_params)
      flash[:notice] = 'Event was successfully updated.'
      redirect_to @event
    else
      render 'edit'
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy

    redirect_to events_path
  end

  def toggle_stand_auction
    @event = Event.find(params[:event][:id])
    params[:event][:stand_auction] = !@event.stand_auction
    if @event.update(event_params)
      respond_to do |format|
        format.json { render :json => {success: true, stand_auction: @event.stand_auction}}
      end
    else
      respond_to do |format|
        format.json { render :json => {success: false}}
      end
    end
  end

  def subscribe_stand
    redirect_to(events_path, :notice => 'Event link not found') and return if params[:hash].nil?
    hashids = Hashids.new("pandorapp",6)
    event_id = hashids.decode(params[:hash])
    @event = Event.find_by_id event_id

    redirect_to(events_path, :notice => 'No existe ningun evento relacionado.') and return unless @event

    redirect_to(events_path, :notice => 'La convocatoria todavia no ha abierto.') and return unless @event.stand_auction

    @reserve = Reserve.find_by(user_id: current_user.id, event_id: @event.id)
    unless @reserve
      @reserve = Reserve.new(user_id: current_user.id, event_id: @event.id)
      @reserve.save
    end
    redirect_to '/wizard/'+@event.id.to_s
  end

  private def generate_hashlinks
    hashids = Hashids.new("pandorapp",6)
    puts @event.id
    @event.stand_hash_link = hashids.encode(@event.id)
    @event.save
  end

  private def event_params
    params.require(:event).permit(:id, :name, :release_date, :stand_auction , :stand_hash_link, :address, :city, :facebook)
  end
end