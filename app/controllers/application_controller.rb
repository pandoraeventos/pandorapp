class ApplicationController < ActionController::Base
  include ApplicationHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  def check_privileges!
    redirect_to "/users", notice: 'You dont have enough permissions to be here' unless current_user.admin
  end

  def load_user
    if user_signed_in?
      @email = current_user.email
      current_user
    end
  end

  def time_left (start_time,end_time)
    days_left = (7*86400 - (end_time.utc - start_time)) / 60 /60 / 24
    hour_left = (days_left - days_left.to_i) * 24
    minutes_left = (hour_left - hour_left.to_i) * 60

    hour_left = hour_left < 10 ? '0'+hour_left.to_i.to_s : hour_left.to_i
    minutes_left = minutes_left < 10 ? '0'+minutes_left.to_i.to_s : minutes_left.to_i

    {days: days_left.to_i, hours: hour_left, minutes: minutes_left, ended: days_left < 0}
  end

  helper_method :time_left
end
