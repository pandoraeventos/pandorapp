class TasksController  < ApplicationController
  layout 'main_app'

  def dashboard
    render component: 'Dashboard'
  end
end