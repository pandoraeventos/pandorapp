class PersonalInformationsController < ApplicationController
  layout 'main_app'
  before_action :authenticate_user!
  before_action :load_user

  def create
    @personal_information = PersonalInformation.new(personal_information_params)
    @personal_information.user_id = current_user.id
    if @personal_information.save
      redirect_to request.referrer
    else
      render 'new'
    end
  end

  def new
    @data_model = PersonalInformation.find_by_user_id(current_user.id)
    redirect_to edit_personal_information_path(@data_model) and return if @data_model
    render 'users/new'
  end

  def edit
    @data_model = PersonalInformation.find_by_user_id(current_user.id)
    redirect_to(users_path, :notice => 'Personal Information not found') and return unless @data_model
    render 'users/edit'
  end

  def update
    @personal_information = PersonalInformation.find(params[:id])

    if @personal_information.update(personal_information_params)
      flash[:notice] = 'Personal Information was successfully updated.'
      if request.referer.include? 'wizard'
        redirect_to request.referer.scan(/\/wizard\/[0-9]+/).last
      else
        redirect_to users_path
      end
    else
      render 'edit'
    end
  end


  private def personal_information_params
    params.require(:personal_information).permit(:id, :name, :last_name, :dni, :contact)
  end
end