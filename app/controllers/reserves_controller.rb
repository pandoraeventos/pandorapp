class ReservesController < ApplicationController
  layout 'main_app'
  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :check_privileges!, only:[:all,:expire,:about_to_expire]
  before_action :load_user
  include MercadoPagoHelper

  def index
    @reserves = Reserve.where(user_id: current_user.id, agreement: true)
  end

  def all
    redirect_to(reserves_path, :notice => 'Evento no encontrado') if params[:event].nil?
    @reserves = Reserve.where(event_id: params[:event])
  end

  def show
    redirect_to(reserves_path, :notice => 'Reserve Id not found') if params[:id].nil?
    @reserve = Reserve.find_by_id(params[:id])
    redirect_to(reserves_path, :notice => 'Reserve Information not found') unless @reserve
    unless @reserve.mail_sent.nil?
      redirect_to('/payment_status/'+@reserve.id.to_s) and return unless @reserve.payment_confirmation.nil?
      @stands = Stand.where(event_id: @reserve.event_id)
      @reserves = Reserve.where(event_id: @reserve.event_id)
      time = time_left @reserve.mail_sent, Time.now
      if time[:hours] <= 0 && time[:minutes] <= 0
        flash[:notice] = 'Han pasado mas de 24 desde que le enviamos el mail de seleccion de Stand, el lugar seleccionado puede que no este disponible a la hora de confirmarlo.'
      else
        flash[:notice] = 'Tiene '+time[:hours].to_s + ':'+ time[:minutes].to_s+ ' hrs de prioridad para elegir con respecto a los stands que reservaron despues de ud.'
      end
    end
  end

  def edit
    unless params[:id].nil?
      @reserve = Reserve.find_by_id params[:id]
      redirect_to(reserves_path, :notice => 'Reserve not found') unless @reserve
    end
  end

  def update
    @reserve = Reserve.find(params[:id])
    @reserve.merchant_id = Merchant.find_by_user_id(current_user.id).id
    @reserve.wait_number = Reserve.where("event_id = ? and wait_number != ?", @reserve.event.id , 0).length + 1 if params[:reserve][:wait_line] == "true"

    if @reserve.update(reserve_params)
      if request.referer.include? 'wizard'

        PandoraMailer.reserve_confirmation(@reserve, request).deliver
        flash[:notice] = 'Gracias por haber solicitado un stand para el evento '+ @reserve.event.name+' pronto le llegara una notificacion via email con los pasos a seguir.'
        redirect_to '/wizard/'+@reserve.event.id.to_s
      else
        redirect_to @reserve
      end
    else
      render 'edit'
    end
  end

  def destroy
    @reserve = Reserve.find(params[:id])
    @reserve.destroy
    redirect_to reserves_path
  end



  def payment
    @reserve = get_reserve
    redirect_to(reserves_path, :notice => 'Reserve not found') unless @reserve

    return if @reserve.nil? || !@reserve.payment_confirmation.nil?
    stand = Stand.find_by_name(params[:stand])
    return if stand.nil?
    @reserve.payment_confirmation = DateTime.now
    @reserve.stand = stand

    create_payment(stand)

    @reserve.save
    redirect_to('/payment_status/'+@reserve.id.to_s)
  end


  def about_to_expire
    @reserve = Reserve.find(params[:id])
    redirect_to(reserves_path, :notice => 'Reserve not found') unless @reserve
    PandoraMailer.reserve_about_to_expire(@reserve,request).deliver
    redirect_to ('/reserves/all/' + @reserve.event.id.to_s)
  end

  def expire
    @reserve = Reserve.find(params[:id])
    redirect_to(reserves_path, :notice => 'Reserve not found') unless @reserve
    PandoraMailer.reserve_expired_backup(@reserve,request).deliver
    @reserve.stand_id = nil
    @reserve.mail_sent = nil
    @reserve.payment_confirmation = nil
    unless @reserve.payment.nil?
      cancel_payment(@reserve.payment.mp_id) unless @reserve.payment.mp_id.nil?
      @reserve.payment.destroy
    end
    @reserve.save
    PandoraMailer.reserve_expired(@reserve,request).deliver
    redirect_to ('/reserves/all/' + @reserve.event.id.to_s)
  end

  private def create_payment(stand)
    payment = Payment.new
    payment.payment_type = params[:payment_type]
    payment.amount = stand.price
    payment.status = 'Pago no efectuado'
    payment.reserve = @reserve
    payment.save
  end

  private def get_reserve
    @reserve = Reserve.where(user_id: current_user.id, id: params[:id]).first
  end

  private def reserve_params
    params.require(:reserve).permit(:id, :payment_id, :agreement, :stand_id, :event_id, :merchant_id, :wait_number)
  end
end