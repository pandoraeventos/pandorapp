class PaymentsController < ApplicationController
  layout 'main_app'

  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :load_user


  def payment_status
    @reserve = get_reserve
    redirect_to(reserves_path, :notice => 'Reserva no encontrada.') and return unless @reserve
    redirect_to(reserves_path, :notice => 'No se registran confirmaciones de la reserva aun.') and return if @reserve.payment_confirmation.nil?
    redirect_to(reserves_path) and return unless @reserve.payment.payment_date.nil?

    render_payment @reserve
  end

  def payment_toggle
    @reserve = get_reserve
    redirect_to(reserves_path, :notice => 'Reserva no encontrada.') and return if @reserve.nil?
    redirect_to(reserves_path, :notice => 'No se registran confirmaciones de la reserva aun.') and return if @reserve.payment_confirmation.nil?


    @reserve.payment.payment_type = @reserve.payment.payment_type == 0? 1 : 0 if @reserve.payment.payment_date.nil?
    @reserve.payment.save

    render_payment @reserve
  end

  def payment_email
    @reserve = Reserve.find(params[:reserve][:id])
    if @reserve.mail_sent.nil?
      PandoraMailer.reserve_payment(@reserve, request).deliver
      @reserve.mail_sent = DateTime.now
      status = 'mail_sent'
    else
      @reserve.mail_sent = nil
      status = 'cancelled'
    end

    @reserve.save
    render json: {status: status}
  end

  def payment_confirm
    @reserve = Reserve.find(params[:id])
    if @reserve.payment.payment_date.nil?
      @reserve.payment.status = 'Finalizado'
      @reserve.payment.payment_date = Time.now
      PandoraMailer.reserve_complete(@reserve, request).deliver
    else
      @reserve.payment.status = @reserve.payment.payment_confirmation_img.nil? ? 'Pago no efectuado' : 'Comprobante enviado.'
      @reserve.payment.payment_date = nil
    end
    @reserve.payment.save
    redirect_to ('/reserves/all/'+@reserve.event_id.to_s)
  end

  def payment_upload
    @reserve = get_reserve
    redirect_to(reserves_path, :notice => 'Reserva no encontrada.') and return unless @reserve
    redirect_to(reserves_path, :notice => 'No se registran confirmaciones de la reserva aun.') and return if @reserve.payment_confirmation.nil?
    redirect_to(reserves_path) and return unless @reserve.payment.payment_date.nil?

    @reserve.payment.payment_confirmation_img = params[:payment_confirmation_img]
    @reserve.payment.status = 'Comprobante enviado.'
    @reserve.payment.save

    #notificacion a pandora
    PandoraMailer.reserve_payment_upload(@reserve, request).deliver
    flash[:notice] = 'Su comprobante ha sido enviado. Una vez comprobado, le notificaremos via email su confirmacion en el evento. Gracias por ser parte de nuestro evento.'

    render_payment @reserve
  end

  private def get_reserve
    @reserve = Reserve.where(user_id: current_user.id, id: params[:id]).first
  end

  private def render_payment(reserve)
    time = time_left(@reserve.payment_confirmation, Time.now)
    flash[:notice] = 'Tiene '+ time[:days].to_s + ' dias ' +time[:hours].to_s + ':'+ time[:minutes].to_s+ ' para efectuar el pago de su reserva.'

    @hide_payment_button = true
    @payment_img = false
    @payment_img = Cloudinary::Api.resource(@reserve.payment.payment_confirmation_img)['url'] unless @reserve.payment.payment_confirmation_img.nil?

    render 'payments/deposit_payment' and return  if @reserve.payment.payment_type == 0
    redirect_to '/mercado_payment/'+@reserve.id.to_s if @reserve.payment.payment_type == 1
  end

  private def payment_params
    params.require(:payment).permit(:id, :payment_confirmation_img)
  end
end