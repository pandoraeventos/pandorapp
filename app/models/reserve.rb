class Reserve < ActiveRecord::Base
  belongs_to :stand
  belongs_to :merchant
  belongs_to :event
  belongs_to :user
  has_one :payment
end
