class Task < ActiveRecord::Base
  belongs_to :event
  belongs_to :user
  belongs_to :creator, :class_name => 'User'
end