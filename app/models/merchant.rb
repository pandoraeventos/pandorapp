class Merchant < ActiveRecord::Base
  belongs_to :personal_information
  belongs_to :user
  belongs_to :category
end
