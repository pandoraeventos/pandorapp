function toggleStandAuction() {
    $.ajax({
        type: "POST",
        url: "/events/toggle_stand_auction",
        data: {
            event: $('.event_class').data('event')
        },
        success: function(msg){
            if (msg.success) {
                $('#stands-auction').html((msg['stand_auction'])?'Detener Convocatoria de Stands':'Iniciar Convocatoria de Stands')
            }
        }
    })
}
$(document).ready(function () {
    $('#stands-auction').on('click',toggleStandAuction);

    $('a').css('text-transform','initial');
});