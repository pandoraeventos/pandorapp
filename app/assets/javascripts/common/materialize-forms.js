$(document).ready(function() {
    $('#submit-button').click(function () {
        $( "input[type='submit']" ).trigger('click')
    });
    $('.input-field').find('.field_with_errors').each(function () {
            $(this).parent().append($(this).html());
            $(this).remove();
        }
    );
    $('select').material_select();
    $('.tooltipped').tooltip({delay: 50});
    $(".button-collapse").sideNav();
});