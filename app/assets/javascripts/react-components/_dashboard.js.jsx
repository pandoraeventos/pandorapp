let Dashboard = React.createClass({
    getInitialState() {
        return {
            tasks: []
        }
    },
    componentDidMount() {
        $.getJSON('/api/v1/tasks', (response) => {
            this.setState({ tasks: response })
        });
    },
    handleSubmit(task) {
        let newState = this.state.tasks.concat(task);
        this.setState({ tasks: newState })
    },

    render() {
        return (
            <div className="Dashboard">
                <div className="task-column">
                    <TasksColumn id='To-Do' tasks={this.state.tasks}/>
                </div>
                <div className="task-column">
                    <TasksColumn id='In-Progress' tasks={[]} />
                </div>
                <div className="task-column">
                    <TasksColumn id='Done' tasks={[]}/>
                </div>
            </div>
        );
    }
});
