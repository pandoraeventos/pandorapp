class Tasks extends React.Component {
    render() {
        var tasks= this.props.tasks.map((task) => {
            return (
                <div key={task.id}>
                    <h3>{task.name}</h3>
                    <p>{task.description}</p>
                </div>
            )
        });
        return(
            <div> {tasks} </div>
        )
    }
}