class TasksColumn extends React.Component {
    render() {
        return (
            <div>
                <h1>{this.props.id}</h1>
                <Tasks tasks={this.props.tasks}/>
            </div>
        );
    }
}
