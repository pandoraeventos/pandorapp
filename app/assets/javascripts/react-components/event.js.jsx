var Event = React.createClass({
  propTypes: {
    name: React.PropTypes.string,
    release_date: React.PropTypes.node
  },
  getInitialState() {
    return { items: [] }
  },
  componentDidMount() {
    //$.getJSON('/api/v1/events', (response) => { this.setState({ items: response }) });
      this.setState({ items: this.props })
  },
  handleSubmit(item) {
    var newState = this.state.items.concat(item); 
    this.setState({ items: newState })
  },

  render: function () {
    return (
      <div>
        <Tabs/>
        <TabsContent />
      </div>
    );
  }
});
