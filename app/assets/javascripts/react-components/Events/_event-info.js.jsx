class EventInfo extends React.Component {
    render() {
        return (
            <div>
                <div>Name: {this.props.name}</div>
                <div>Release Date: {this.props.release_date}</div>
            </div>
        );
    }
}