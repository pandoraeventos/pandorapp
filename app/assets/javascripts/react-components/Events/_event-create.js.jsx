var EventCreate = React.createClass({
  handleClick() {
    var name = this.refs.name.value;
    var release_date = this.refs.release_date.value;
    var data = { "name": name, "release_date": release_date };
    data = JSON.stringify(data);
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "/api/v1/events",
      "method": "POST",
      "headers": {
        "content-type": "application/json"
      },
      "processData": false,
      "data": data
    }

    $.ajax(settings).done(function (response) {
      (item) => { this.props.handleSubmit(item); }
      console.log(response);
    });
  }, render() {
    return (
      <div>
        <input ref='name' placeholder='Enter the name of the item' />
        <input type='date' ref='release_date' placeholder='Enter a release_date' />
        <button onClick={this.handleClick}>Submit</button>
      </div>
    )
  }
});