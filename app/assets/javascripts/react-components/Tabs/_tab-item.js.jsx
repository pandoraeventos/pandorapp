class TabItem extends React.Component {
    render() {
        return (
                <li className="nav-item">
                    <a className={this.props.class} id={this.props.id} data-toggle="tab" href={this.props.href} role="tab" aria-controls={this.props.aria} aria-selected="true">{this.props.label}</a>
                </li>
        );
    }
}

