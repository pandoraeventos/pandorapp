class TabsContent extends React.Component {
    render() {
        return (
            <div className="tab-content" id="eventTabContent">
                <div className="tab-pane fade show active" id="create" role="tabpanel" aria-labelledby="create-tab">
                    <EventCreate handleSubmit={this.handleSubmit} />
                </div>
                <div className="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
                    <EventInfo />
                </div>
                <div className="tab-pane fade" id="stands" role="tabpanel" aria-labelledby="stands-tab">
                    <EventStands />
                </div>
                <div className="tab-pane fade" id="concursos" role="tabpanel" aria-labelledby="concursos-tab">
                    <EventContests />
                </div>
            </div>
        );
    }
}