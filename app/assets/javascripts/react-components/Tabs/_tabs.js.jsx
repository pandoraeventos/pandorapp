class Tabs extends React.Component {
    render() {
        return (
            <ul className="nav nav-tabs" id="event-tab" role="tablist">
                <TabItem id="create-tab" href="#create" aria="create" label="Create" class='nav-item nav-link active'/>
                <TabItem id="info-tab" href="#info" aria="info" label="Info" class='nav-item nav-link'/>
                <TabItem id="stands-tab" href="#stands" aria="stands" label="Stands" class='nav-item nav-link'/>
                <TabItem id="concursos-tab" href="#concursos" aria="concursos" label="Concursos" class='nav-item nav-link'/>
            </ul>
        );
    }
}