class Navigator extends React.Component {
    render() {
        return (
            <div>
                <a href={this.props.url}>
                    <i className={this.props.icon} aria-hidden="true"/>
                    <br/>
                    <span>{this.props.title}</span>
                </a>
            </div>
        );
    }
}