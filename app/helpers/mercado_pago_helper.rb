module MercadoPagoHelper
  require 'mercadopago'
  ENVIRONMENT = ENV['APPLICATION_HOST'] || 'http://www.pandoraboxeventos.com'

  private def init_mp
    MercadoPago.new(ENV['CLIENT_ID'], ENV['CLIENT_SECRET'])
  end

  def create_payment_token(reserve)
    $mp = init_mp
    preference_data = {
      "items": [
        { "id": reserve.id,
          "title": "STAND GEEK-CON #3", 
          "description": "Stand Nº"+ reserve.stand.name + " Zona "+ (reserve.stand.sector == 'scenario'? 'Escenario' : 'Salas Tematicas'),
          "picture_url": "http://res.cloudinary.com/dktk7tmxj/image/upload/v1516249109/Geek-Con_plwwse.jpg",
          "quantity": 1,
          "unit_price": reserve.stand.price,
          "currency_id": "ARS"
        }
      ],
      "payer": {
          "name": reserve.user.personal_information.name,
          "email": reserve.user.email,
          "identification": {
                "type": "DNI",
                "number": reserve.user.personal_information.dni
          }
      },
      "back_urls": {
        "success": ENVIRONMENT + '/mercado_pago/success', #request url
        "failure": ENVIRONMENT + '/mercado_pago/failure',
        "pending": ENVIRONMENT + '/mercado_pago/pending'
      },
      "notification_url": ENVIRONMENT + '/mercado_pago/ipn', #request url
      "external_reference": reserve.id,
      "expires": true,
      "expiration_date_from": DateTime.parse(@reserve.payment_confirmation.to_s).iso8601(3), #fecha confirmacion reserva
      "expiration_date_to": DateTime.parse((@reserve.payment_confirmation + 7.days).to_s).iso8601(3)   #fecha confirmacion reserva + 7
    }
    $mp.create_preference(preference_data)
    #test_token
  end

  def get_preference_token(id)
    $mp = init_mp
    $mp.get_preference(id)
    #test_token
  end

  def get_payment_info(id)
    $mp = init_mp
    payment_info = $mp.get_payment_info(id)
  end

  def cancel_payment(id)
    $mp = init_mp
    $mp.cancel_payment(id)
  end

  def test_token
    {"status"=>"201", "response"=>{"collector_id"=>45175426, "operation_type"=>"regular_payment", "items"=>[{"id"=>"15", "picture_url"=>"http://res.cloudinary.com/dktk7tmxj/image/upload/v1516249109/Geek-Con_plwwse.jpg", "title"=>"STAND GEEK-CON #3", "description"=>"Stand NºP1 Zona Escenario", "category_id"=>"", "currency_id"=>"ARS", "quantity"=>1, "unit_price"=>650}], "payer"=>{"name"=>"David", "surname"=>"", "email"=>"david.roth.1987@gmail.com", "date_created"=>"", "phone"=>{"area_code"=>"", "number"=>""}, "identification"=>{"type"=>"DNI", "number"=>"33102980"}, "address"=>{"street_name"=>"", "street_number"=>nil, "zip_code"=>""}}, "back_urls"=>{"success"=>"http://www.pandoraboxeventos.com/mercado_payment/success", "pending"=>"http://www.pandoraboxeventos.com/mercado_payment/pending", "failure"=>"http://www.pandoraboxeventos.com/mercado_payment/failure"}, "auto_return"=>"", "payment_methods"=>{"excluded_payment_methods"=>[{"id"=>""}], "excluded_payment_types"=>[{"id"=>""}], "installments"=>nil, "default_payment_method_id"=>nil, "default_installments"=>nil}, "client_id"=>"963", "marketplace"=>"NONE", "marketplace_fee"=>0, "shipments"=>{"receiver_address"=>{"zip_code"=>"", "street_number"=>nil, "street_name"=>"", "floor"=>"", "apartment"=>""}}, "notification_url"=>"http://www.pandoraboxeventos.com/mercadopago/ipn", "external_reference"=>"15", "additional_info"=>"", "expires"=>true, "expiration_date_from"=>"2018-01-24T01:56:57.000-04:00", "expiration_date_to"=>"2018-01-31T01:56:57.000-04:00", "date_created"=>"2018-01-24T02:52:38.642-04:00", "id"=>"45175426-dd0d0c24-9313-4e11-950c-bc3b5d9d10ae", "init_point"=>"https://www.mercadopago.com/mla/checkout/start?pref_id=45175426-dd0d0c24-9313-4e11-950c-bc3b5d9d10ae", "sandbox_init_point"=>"https://sandbox.mercadopago.com/mla/checkout/pay?pref_id=45175426-dd0d0c24-9313-4e11-950c-bc3b5d9d10ae"}}
  end

  def test_payment
    {"collection" => { "id" => 3382192046, "site_id" => "MLA", "date_created" => "2018-01-24T16:56:36.000-04:00", "date_approved" => nil, "money_release_date" => nil, "last_modified" => "2018-01-24T16:56:36.000-04:00", "payer" => { "id" => 114063204, "first_name" => "David", "last_name" => "Roth", "phone" => { "area_code" => " ", "number" => "2494317403", "extension" => "" }, "identification" => { "type" => "DNI", "number" => "33102980" }, "email" => "david.roth.1987@gmail.com", "nickname" => "ROTHD2012" }, "order_id" => "15", "external_reference" => "34", "merchant_order_id" => 644619615, "reason" => "STAND GEEK-CON #3", "currency_id" => "ARS", "transaction_amount" => 650, "net_received_amount" => 0, "total_paid_amount" => 650, "shipping_cost" => 0, "coupon_amount" => 0, "coupon_fee" => 0, "finance_fee" => 0, "discount_fee" => 0, "coupon_id" => nil, "status" => "approved", "status_detail" => "pending_waiting_payment", "issuer_id" => nil, "installment_amount" => 0, "deferred_period" => nil, "payment_type" => "ticket", "marketplace" => "NONE", "operation_type" => "regular_payment", "marketplace_fee" => 0, "deduction_schema" => nil, "refunds" => [ ], "amount_refunded" => 0, "last_modified_by_admin" => nil, "api_version" => "2", "concept_id" => nil, "concept_amount" => 0, "internal_metadata" => { "tags" => [ "payment_option_id-pagofacil" ], "processed_by" => "checkout-off-v1" }, "collector" => { "id" => 45175426, "first_name" => "Eduardo Hector", "last_name" => "Rodriguez", "phone" => { "area_code" => "0249", "number" => "154685444", "extension" => "" }, "email" => "martemars2000@hotmail.com", "nickname" => "YAMI-MARS" } } }
  end
end