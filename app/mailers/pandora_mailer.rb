class PandoraMailer < ActionMailer::Base
  helper :application # gives access to all helpers defined within `application_helper`.
  default to: 'pandorasboxeventos@gmail.com'

  def contact_email (name, email, body, request)
    @name = name
    @email = email
    @body = body
    @request = request
    mail(from: email, subject: 'Mail de Contacto desde Pandora Web')
  end

  def reserve_confirmation (reserve, request)
    @email = reserve.user.email
    @event = reserve.event.name
    @reserve = reserve.id
    @request = request
    mail(to: @email, from: 'info@pandorasboxeventos.com', subject: 'Su solicitud de stand ha sido registrada')
  end

  def reserve_payment (reserve, request)
    @email = reserve.user.email
    @event = reserve.event.name
    @reserve = reserve.id
    @request = request
    mail(to: @email, from: 'info@pandorasboxeventos.com', subject: 'Su stand ha sido seleccionado para Geek Con #3')
  end

  def reserve_payment_upload (reserve, request)
    @event = reserve.event.name
    @reserve = reserve.id
    @merchant = reserve.merchant.store_name
    @request = request
    mail(from: 'info@pandorasboxeventos.com', subject: 'Un stand a subido su comprobante de pago o pago en ML')
  end

  def reserve_complete (reserve, request)
    @email = reserve.user.email
    @event = reserve.event.name
    @reserve = reserve.id
    @request = request
    mail(to: @email, from: 'info@pandorasboxeventos.com', subject: 'Su pago ha sido comprobado para Geek Con #3')
  end

  def reserve_about_to_expire (reserve, request)
    @email = reserve.user.email
    @event = reserve.event.name
    @reserve = reserve.id
    @request = request
    mail(to: @email, from: 'info@pandorasboxeventos.com', subject: 'Su tiempo de reserva esta por terminar.')
  end

  def reserve_expired (reserve, request)
    @email = reserve.user.email
    @event = reserve.event.name
    @reserve = reserve.id
    @request = request
    mail(to: @email, from: 'info@pandorasboxeventos.com', subject: 'Su tiempo de reserva ha caducado por falta de pago.')
  end

  def reserve_expired_backup (reserve, request)
    @email = reserve.user.email
    @event = reserve.event.name
    @reserve = reserve
    @request = request
    mail(from: 'info@pandorasboxeventos.com', subject: 'Una Reserva ha caducado por falta de pago.')
  end
end